package main
import (
  "fmt"
  "net/http"
  "strings"
  "math/rand"
)
func sayHello(w http.ResponseWriter, r *http.Request) {
  message := r.URL.Path
  message = strings.TrimPrefix(message, "/")
  list := rand.Perm(20)
  res := fmt.Sprintf("%v", list)
  message = "<h1>The final draw: " + res + "</h1>"
  w.Write([]byte(message))
}
func main() {
  http.HandleFunc("/", sayHello)
  if err := http.ListenAndServe(":9405", nil); err != nil {
    panic(err)
  }
}
