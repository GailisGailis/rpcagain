// Formålet er å kunne lage, hente, redigere og
// slette elementer fra en liste med gjøremål
// Implementasjon er basert på "Creating an RPC
// Server in Go", by Simon, Apr 19, 2018
// URL: https://medium.com/@OmisNomis/
//      creating-an-rpc-server-in-go-3a94797ab833
package main

import (
	"log"
)

// Lag en ny ToDo type som er en (typed) kolleksjon 
// av felt. 
// Hver ToDo skal i første omgang ha Title og Status
// Begge skal kunne holde tekst, derfor velger man
// den innebygde typen i Golang, - string
type ToDo struct {
	Title, Status string
}

// Denne variabelen, "todoslice", som kan holde 
// instanser av typen ToDo
// En liste med ToDo instanser / elementer
//    Legg merke til hvordan en liste kan deklareres
//    i Golang, - med hakkeparanteser (sjekk ordet)
var todoSlice []ToDo 

// Denne funksjonen, GetToDo, har en string (tekst)
// som inn-data/in-parameter og returnerer et 
// ToDo-element (instanse)
func GetToDo(title string) ToDo {

}

// Lag en ny liste for lagring av gjøremål (ToDo 
// elementene). 


func main() {
	// Programmet starter utførelse her
}
